'use strict';
var currentTime = new Date();
var productURL = ["m-dial-brown","m-strap-brown","m-case-brown"];


function selectClock(src) {
    return repaint(
        src.substring(src.lastIndexOf("/")+1, src.lastIndexOf(".")),
        productURL[1],
        productURL[2]);
}

function selectBand(src) {
    return repaint(
        productURL[0],
        src.substring(src.lastIndexOf("/")+1,src.lastIndexOf(".")),
        productURL[2]);
}

function selectCase(src) {
    return repaint(
        productURL[0],
        productURL[1],
        src.substring(src.lastIndexOf("/")+1,src.lastIndexOf(".")));
}

function randomWatch() {
    var caseArray = [],dialArray = [],strapArray = [];
    var i;
    for (i=0;i<document.images.length;i++){
        if (document.images[i].src.indexOf("_")===-1) {
            if (document.images[i].src.indexOf("-case-") !== -1)
                caseArray.push(document.images[i].src.slice(document.images[i].src.lastIndexOf("/") + 1, document.images[i].src.lastIndexOf(".")));
            if (document.images[i].src.indexOf("-dial-") !== -1)
                dialArray.push(document.images[i].src.slice(document.images[i].src.lastIndexOf("/") + 1, document.images[i].src.lastIndexOf(".")));
            if (document.images[i].src.indexOf("-strap-") !== -1)
                strapArray.push(document.images[i].src.slice(document.images[i].src.lastIndexOf("/") + 1, document.images[i].src.lastIndexOf(".")));
        }
    }
    repaint(
        dialArray[parseInt(Math.random() * dialArray.length)],
        strapArray[parseInt(Math.random() * strapArray.length)],
        caseArray[parseInt(Math.random() * caseArray.length)]);
}


function repaint(clocks,band,wCase) {
    //Unmark border
    if (productURL[0]!==clocks || productURL[1]!==band || productURL[2]!==wCase){
        document.getElementById(productURL[0]).style.borderBottom = '3px solid transparent';
        document.getElementById(productURL[1]).style.borderBottom = '3px solid transparent';
        document.getElementById(productURL[2]).style.borderBottom = '3px solid transparent';
    }
    //Mark border
    document.getElementById(clocks).style.borderBottom = '3px solid '+ clocks.slice(clocks.lastIndexOf("-") + 1);
    document.getElementById(band).style.borderBottom = '3px solid '+ band.slice(band.lastIndexOf("-") + 1);
    document.getElementById(wCase).style.borderBottom = '3px solid '+ wCase.slice(wCase.lastIndexOf("-") + 1);
    //Image and link modification
    document.all.watchSample.src = "../../watchCustomization/images/" +clocks + "_" + band + "_" + wCase + ".png";
    document.all.submitLink.href = "../product/" + clocks + "_" + band + "_" + wCase;
    log();
    return productURL = [clocks,band,wCase];
}

randomWatch();


function log() {
    var ob = ("Date : " + new Date() + " Product : " + productURL.join("_") + " Time selected : " + new Date(new Date() - currentTime).getSeconds() + "s User System : " + navigator.platform );
    currentTime = new Date();
    $.ajax({
        url: '../../watchCustomization/log.php',
        data : 'postVar=' + ob,
        type : "POST"
    });
}


